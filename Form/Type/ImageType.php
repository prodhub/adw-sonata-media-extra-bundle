<?php

namespace ADW\SonataMediaExtraBundle\Form\Type;

use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImageType extends MediaType
{
    const ALLOWED_MIMES = 'image/gif,image/jpeg,image/pjpeg,image/png,image/bmp';

    const RESIZE_TYPE__INSET = 'inset';
    const RESIZE_TYPE__OUTBOUND = 'outbound';

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // Translate base64 data ot UploadedFile for binaryContent
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $ed = $event->getForm()->getExtraData();

            if (!isset($ed['base64String'])) {
                @trigger_error(
                    'You forgot to include twig file with widgets from ADWSonataMediaExtraBundle',
                    E_USER_WARNING
                );
                return;
            }

            /** @var Media $media */
            $media = $event->getData();
            if (null == $media) {
                return;
            }

            /** @var UploadedFile $originalFile */
            $originalFile = $media->getBinaryContent();
            if (null == $originalFile) {
                return;
            }

            $base64 = $ed['base64String'];
            $base_to_php = explode(',', $base64);

            $mimeSuffix = $this->getMimeSuffix($base_to_php[0]);

            $ext = ($mimeSuffix == 'png') ? 'png' : 'jpg';
            $fileName = md5($originalFile->getClientOriginalName() . rand(0, 100000)) . '.' . $ext;
            $filePath = sys_get_temp_dir() . '/' . $fileName;

            $data = base64_decode($base_to_php[1]);
            file_put_contents($filePath, $data);

            $uf = new UploadedFile($filePath, $originalFile->getClientOriginalName(), null, null, null, true);
            $media->setBinaryContent($uf);
        });
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class' => $this->class,
            'new_on_update' => false,
            'resize_type' => self::RESIZE_TYPE__INSET,
            'allowed_mime_types' => explode(',', self::ALLOWED_MIMES),
            'canvas' => ['width' => 320, 'height' => 320,],
        ]);

        return $resolver;
    }

    /**
     * @inheritDoc
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * @inheritDoc
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        if (
            isset($options['canvas']['width'])
            && intval($options['canvas']['width']) > 0
            && isset($options['canvas']['height'])
            && intval($options['canvas']['height']) > 0
        ) {
            $view->vars['resize_type'] = $options['resize_type'];
        } elseif (
            (!isset($options['canvas']['width']) || intval($options['canvas']['width']) <= 0)
            && (!isset($options['canvas']['height']) || intval($options['canvas']['height']) <= 0)
        ) {
            throw new \LogicException('Expect one of params "width" and "height". No one specified correctly!');
        } else {
            if (isset($options['resize_type'])) {
                @trigger_error('You should not set "resize_type" option when to use one of dimension (width or 
                height)');
            }
            $view->vars['resize_type'] = ImageType::RESIZE_TYPE__OUTBOUND;
        }
        $view->vars['allowed_mime_types'] = $options['allowed_mime_types'];
        $view->vars['canvas'] = $options['canvas'];
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adw_sonata_media_extra__image_type';
    }

    /**
     * @param $head
     * @return mixed
     */
    private function getMimeSuffix($head)
    {
        $ms = explode(':', $head)[1];
        $ms = explode('/', $ms)[1];
        $ms = explode(';', $ms)[1];

        return $ms;
    }
}