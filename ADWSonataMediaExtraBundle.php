<?php

namespace ADW\SonataMediaExtraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ADWSonataMediaExtraBundle extends Bundle
{
    const ADMIN_STUB_IMG_URI = '/bundles/adwsonatamediaextra/images/admin/aws-video-stub.png';
    const CUT_VIDEO_FRAME_NUMBER = 10;
    const RESIZE_SCREENSHOT_FRAME_WIDTH = 320;
    const RESIZE_SCREENSHOT_FRAME_HEIGHT = 200;
}
