<?php

namespace ADW\SonataMediaExtraBundle\Tests\Filesystem\Adapter;

use ADW\SonataMediaExtraBundle\Filesystem\Adapter\AwsET;
use Aws\S3\S3Client;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

class AwsETTest extends WebTestCase
{
    /** @var Client */
    protected $client = null;

    private $fileName = null;

    private $key = 'images/test/delete-me-please.png';

    /** @var AwsET */
    private $adapter;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();

        $this->fileName = realpath(__DIR__ . '/../../../Resources/public/images/admin/aws-video-stub.png');

        $container = $this->client->getContainer();
        $fsAwsEt = $container->getParameter('adw_sonata_media_extra.config.filesystem.aws_et');
        $presetAwsEt = $container->getParameter('adw_sonata_media_extra.config.aws_et_preset');

        $awsOptions = [
            'credentials' => [
                'secret' => $fsAwsEt['secretKey'],
                'key' => $fsAwsEt['accessKey'],
            ],
            'region' => $fsAwsEt['region'],
            'version' => $fsAwsEt['version'],
            'directory' => $fsAwsEt['directory'],
        ];

        $presetName = $fsAwsEt['preset'];

        $this->adapter = new AwsET(
            new S3Client($awsOptions),
            $fsAwsEt['bucket_input'],
            $fsAwsEt['bucket_output'],
            array_merge($awsOptions, [
                'pipeline' => $fsAwsEt['pipeline'],
                'preset' => $presetAwsEt[$presetName],
            ])
        );
    }

    public function testLifeCircle()
    {
        $existsBU = $this->adapter->existsInput($this->key);
        $sgz = $this->adapter->write($this->key, file_get_contents($this->fileName)) > 0;
        sleep(5);
        $existsAU = $this->adapter->existsInput($this->key);
        $del = $this->adapter->deleteInput($this->key);
        $existsAD = $this->adapter->existsInput($this->key);

        $this->assertTrue(
            !$existsBU && $sgz && $existsAU && $del && !$existsAD,
            'Configuration for ASW ET is invalid!');
    }
}
