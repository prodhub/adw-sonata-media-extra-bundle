<?php

namespace ADW\SonataMediaExtraBundle\Test;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * It help you to apply file uploading for form type \ADW\SonataMediaExtraBundle\Form\Type\ImageType
 */
trait ExtraUploadWebTestCaseTrait
{
    /**
     * Upload image like base64 to form field
     *
     * @param array $field
     * @param string $filename
     * @param null|string $originName
     * @param string $mimeType
     */
    private function uploadBase64(array $field, $filename, $originName = null, $mimeType = 'image/jpeg')
    {
        $base64Prefix = 'data:image/jpeg;base64,';
        $value = $base64Prefix . base64_encode(file_get_contents($filename));
        $field['base64String']->setValue($value);

        $image = new UploadedFile($filename, $originName, $mimeType);
        $field['binaryContent']->upload($image);
    }
}