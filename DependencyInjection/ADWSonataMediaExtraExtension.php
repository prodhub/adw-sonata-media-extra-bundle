<?php

namespace ADW\SonataMediaExtraBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ADWSonataMediaExtraExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        if (isset($config['cdn']) && isset($config['cdn']['aws_et'])
            && isset($config['filesystem']) && isset($config['filesystem']['aws_et'])
            && $config['aws_et_preset']
        ) {
            $this->configureAwsET($container, $config);
        }

        $container->getDefinition('adw_sonata_media_extra.image_type')
            ->replaceArgument(1, $config['class']['media']);

        if (
            isset($config['filesystem']) && isset($config['filesystem']['sftp'])
            && isset($config['cdn']) && isset($config['cdn']['sftp'])
        ) {
            $this->configureSftpConnector(
                $container,
                $config['filesystem']['sftp'],
                $config['cdn']['sftp']
            );
        }
    }

    /**
     * Initialize CDN services
     *
     * @param ContainerBuilder $container
     * @param array $filesystem
     * @param array $cdn
     */
    private function configureSftpConnector(
        ContainerBuilder $container,
        array $filesystem,
        array $cdn
    ) {
        $container->setDefinition(
            'adw_sonata_media_extra.adapter.filesystem.sftp',
            (new Definition(
                'ADW\SonataMediaExtraBundle\Filesystem\Adapter\SftpAdapter',
                [
                    $filesystem['host'],
                    $filesystem['port'],
                    $filesystem['username'],
                    $filesystem['password'],
                    $filesystem['directory'],
                    $filesystem['create'],
                ]
            ))->addMethodCall('setLogger', [new Reference('logger')])
        );

        $container->setDefinition(
            'adw_sonata_media_extra.filesystem.sftp',
            new Definition(
                'Gaufrette\Filesystem',
                [
                    new Reference('adw_sonata_media_extra.adapter.filesystem.sftp'),
                ]
            )
        );

        $container->setDefinition(
            'adw_sonata_media_extra.cdn.sftp',
            new Definition(
                'Sonata\MediaBundle\CDN\Server',
                [
                    $cdn['path'],
                ]
            )
        );
    }

    /**
     * @param ContainerBuilder $container
     * @param $config
     */
    private function configureAwsET(ContainerBuilder $container, array $config)
    {
        $container->setParameter($this->getAlias() . '.config.filesystem.aws_et', $config['filesystem']['aws_et']);
        $container->setParameter($this->getAlias() . '.config.aws_et_preset', $config['aws_et_preset']);

//        if ($container->hasDefinition('adw_sonata_media_extra.adapter.filesystem.aws_et')) {
        $presetName = $config['filesystem']['aws_et']['preset'];

        $container->getDefinition('adw_sonata_media_extra.adapter.filesystem.aws_et')
            ->replaceArgument(0, new Reference('adw_sonata_media_extra.adapter.service.s3'))
            ->replaceArgument(1, $config['filesystem']['aws_et']['bucket_input'])
            ->replaceArgument(2, $config['filesystem']['aws_et']['bucket_output'])
            ->replaceArgument(3, array(
                'credentials' => array(
                    'secret' => $config['filesystem']['aws_et']['secretKey'],
                    'key' => $config['filesystem']['aws_et']['accessKey'],
                ),
//                    'create' => $config['filesystem']['s3']['create'],
                'region' => $config['filesystem']['aws_et']['region'],
                'version' => $config['filesystem']['aws_et']['version'],
                'directory' => $config['filesystem']['aws_et']['directory'],
                'ACL' => 'public',

                'pipeline' => $config['filesystem']['aws_et']['pipeline'],
                'preset' => $config['aws_et_preset'][$presetName],
            ));

        $container->getDefinition('adw_sonata_media_extra.adapter.service.s3')
            ->replaceArgument(0, array(
                'credentials' => array(
                    'secret' => $config['filesystem']['aws_et']['secretKey'],
                    'key' => $config['filesystem']['aws_et']['accessKey'],
                ),
                'region' => $config['filesystem']['aws_et']['region'],
                'version' => $config['filesystem']['aws_et']['version'],
            ));

        $container->getDefinition('adw_sonata_media_extra.cdn.aws_et')
            ->replaceArgument(0, $config['cdn']['aws_et']['path']);
//        }
    }
}
