<?php

namespace ADW\SonataMediaExtraBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_sonata_media_extra');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()

                ->arrayNode('class')->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('media')
                        ->defaultValue('Application\\Sonata\\MediaBundle\\Entity\\Media')
                    ->end()
                ->end()
                ->end()

                ->arrayNode('cdn')
                ->children()
                    ->arrayNode('aws_et')
                    ->children()
                        ->scalarNode('path')->defaultNull()->end()
                    ->end()
                    ->end() // aws_et

                    ->arrayNode('sftp')
                    ->children()
                        ->scalarNode('path')->defaultNull()->end()
                    ->end()
                    ->end() // sftp
                ->end()
                ->end() // cdn
    
                ->arrayNode('filesystem')
                ->children()
                    ->arrayNode('aws_et')
                    ->children()
                        ->scalarNode('pipeline')->defaultNull()->end()
                        ->scalarNode('local_path')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('web_local_path')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('preset')->defaultNull()->end()
                        ->scalarNode('bucket_input')->defaultNull()->end()
                        ->scalarNode('bucket_output')->defaultNull()->end()
                        ->scalarNode('accessKey')->defaultNull()->end()
                        ->scalarNode('secretKey')->defaultNull()->end()
                        ->scalarNode('region')->defaultNull()->end()
                        ->scalarNode('version')->defaultValue('latest')->end()
                        ->scalarNode('directory')->defaultNull()->end()
                        ->scalarNode('acl')->defaultValue('public')->end()
                        ->scalarNode('create')->defaultValue(false)->end()
                    ->end()
                    ->end() // aws_et


                    ->arrayNode('sftp')
                    ->children()
                        ->scalarNode('host')->defaultNull()->end()
                        ->scalarNode('port')->defaultValue(22)->end()
                        ->scalarNode('username')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('password')->defaultNull()->end()
                        ->scalarNode('directory')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('create')->defaultValue(false)->end()
                    ->end()
                    ->end() // aws_et
                ->end()
                ->end() // filesystem
    
                ->arrayNode('aws_et_preset')->useAttributeAsKey('key')->prototype('array')
                ->children()
                    ->scalarNode('id')->isRequired()->end()
                    ->scalarNode('container')->isRequired()->end()
                ->end()
                ->end()

            ->end()
            ->end();

        return $treeBuilder;
    }
}
