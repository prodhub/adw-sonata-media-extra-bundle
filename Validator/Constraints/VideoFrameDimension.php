<?php

namespace ADW\SonataMediaExtraBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class VideoFrameDimension extends Constraint
{
    public $minMessage = 'This value should be {{ limit }} or more.';
    public $maxMessage = 'This value should be {{ limit }} or less.';
    public $invalidMessage = 'This value should be a valid number.';

    public $minWidth;
    public $maxWidth;

    public $minHeight;
    public $maxHeight;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->minWidth && null === $this->maxWidth
            && null === $this->minHeight && null === $this->maxHeight
        ) {
            throw new MissingOptionsException(
                sprintf(
                    'Either option "minWidth" or "maxWidth" or "minHeight" or "maxHeight" must be given for constraint %s',
                    __CLASS__
                ),
                array('minWidth', 'maxWidth', 'minHeight', 'maxHeight')
            );
        }
    }
}