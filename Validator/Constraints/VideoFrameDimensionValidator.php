<?php

namespace ADW\SonataMediaExtraBundle\Validator\Constraints;

use ffmpeg_movie;
use Sonata\MediaBundle\Model\Media;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class VideoFrameDimensionValidator extends ConstraintValidator
{
    /**
     * @param mixed $value The value that should be validated
     * @param Constraint|VideoFrameDimension $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (null === $value) {
            return;
        }

        $minWidth = $constraint->minWidth;
        $maxWidth = $constraint->maxWidth;

        $minHeight = $constraint->minHeight;
        $maxHeight = $constraint->maxHeight;

        if (!($value instanceof Media)) {
            throw new UnexpectedTypeException(
                'This constraint validator applicable only for Media objects!',
                'Sonata\MediaBundle\Model\Media'
            );
        }

        $media = clone $value;
        $binaryContent = $media->getBinaryContent();

        if (null == $binaryContent || !($binaryContent instanceof UploadedFile)) {
            return;
        }

        $mov = new ffmpeg_movie($binaryContent->getRealPath(), false);
        $frameWidth = $mov->getFrameWidth();
        $frameHeight = $mov->getFrameHeight();


        if (null !== $maxWidth && $frameWidth > $maxWidth) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameWidth, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($maxWidth, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameWidth, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($maxWidth, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            }

            return;
        }
        if (null !== $minWidth && $frameWidth < $minWidth) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameWidth, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($minWidth, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameWidth, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($minWidth, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            }
        }


        if (null !== $maxHeight && $frameHeight > $maxHeight) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameHeight, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($maxHeight, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameHeight, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($maxHeight, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            }

            return;
        }
        if (null !== $minHeight && $frameHeight < $minHeight) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameHeight, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($minHeight, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($frameHeight, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($minHeight, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            }
        }
    }
}