<?php

namespace ADW\SonataMediaExtraBundle\Validator\Constraints;

use ffmpeg_movie;
use Sonata\MediaBundle\Model\Media;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class VideoDurationValidator extends ConstraintValidator
{
    /**
     * @param Media $value The value that should be validated
     * @param Constraint|VideoDuration $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (null === $value) {
            return;
        }

        $min = $constraint->min;
        $max = $constraint->max;

        if (!($value instanceof Media)) {
            throw new UnexpectedTypeException(
                'This constraint validator applicable only for Media objects!',
                'Sonata\MediaBundle\Model\Media'
            );
        }

        $media = clone $value;
        $binaryContent = $media->getBinaryContent();

        if (null == $binaryContent || !($binaryContent instanceof UploadedFile)) {
            return;
        }

        $mov = new ffmpeg_movie($binaryContent->getRealPath(), false);
        $duration = $mov->getDuration();

        if (null !== $constraint->max && $duration > $max) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($duration, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($max, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->maxMessage)
                    ->setParameter('{{ value }}', $this->formatValue($duration, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($max, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_HIGH_ERROR)
                    ->addViolation();
            }

            return;
        }

        if (null !== $constraint->min && $duration < $min) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($duration, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($min, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->minMessage)
                    ->setParameter('{{ value }}', $this->formatValue($duration, self::PRETTY_DATE))
                    ->setParameter('{{ limit }}', $this->formatValue($min, self::PRETTY_DATE))
                    ->setCode(VideoDuration::TOO_LOW_ERROR)
                    ->addViolation();
            }
        }
    }
}