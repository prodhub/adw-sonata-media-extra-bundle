# ADW Sonata Media Extra Bundle #


### Важно!

Если Вы хотите использовать функционал для интеграции с Amazon, подключите в проекте эти зависимости.

    "aws/aws-sdk-php": "^3.20",
    "codescale/ffmpeg-php": "^2.7"


### Для чего нужен бандл? ###

Первоначальное назначение данного бандла - 
расширить набор сервисов типа Filesystem, CDNInterface, FilesystemAdapter,
предоставляемых бандлом SonataMedia с целью использования данного бандла
для заливки видео на Amazon S3.

Загвоздка была в том, что мы планировали использовать 
AWS Elastic Transcoder [AWS Elastic Transcoder](https://aws.amazon.com/ru/elastictranscoder/), 
а SonataMedia по умолчанию не поддерживает хранение файлов типа "записать в один бакет, читать из другого".

**Добавлено!!!** Теперь в этом бандле появился форм-тайп для мгновенного отображения превью картинки!


#### В результате имеет: ####

* новый Filesystem Adapter (ADW\SonataMediaExtraBundle\Filesystem\Adapter\AwsET), который после записи в бакет запускает задачу конвертирования видео
* новый Provider (ADW\SonataMediaExtraBundle\Provider\AWSTranscodedVideoFileProvider), который подменяет имя загруженного файла, на имя сконвертированного файла
* friendly-конфигурацию нашего бандла, которая позволяет настроить управление файлами с использованием AWS E T


### Полная конфигурация ###

    adw_sonata_media_extra:
        cdn:
            aws_et:
                path: 'https://s3-%s3_region%.amazonaws.com/%s3_bucket_out_name%'
        filesystem:
            aws_et:
                pipeline:       '1482869066203-d8vpln'
                preset:         'System preset: Web'
                bucket_input:   '%s3_bucket_in_name%'
                bucket_output:  '%s3_bucket_out_name%'
                local_path:     '%kernel.root_dir%/../web/uploads/media'
                web_local_path: '/uploads/media'
                accessKey:      '%s3_access_key%'
                secretKey:      '%s3_secret_key%'
                region:         '%s3_region%'
        aws_et_preset:
            'System preset: Web':
                id:         '1351620000001-100070'
                container:  'mp4'
            'System preset: Gif (Animated)':
                id:         '1351620000001-100200'
                container:  'gif'

### Комментарии ###
* local_path - путь относительно файловой системы к директории, где хранятся миниатюры загруженных роликов.
* web_local_path - путь до данной директории через веб.

### Зависимости ###

* aws/aws-sdk-php: ^3.20
* sonata-project/media-bundle: ~3.1
* codescale/ffmpeg-php: ^2.7

### Запуск тестов ###

    $ wget https://phar.phpunit.de/phpunit-4.8.27.phar && mv phpunit-4.8.27.phar phpunit.phar
    $ chmod +x phpunit.phar && sudo mv phpunit.phar /usr/local/bin/phpunit
    $ phpunit --version
    
    $ phpunit -c app/

### Подключение к проекту ###

    {
        ...
        "require": {
            ...
            "adw/sonata-media-extra-bundle": "dev-master"
        },
        "repositories": [
            ...
            {
                "type": "vcs", 
                "url": "https://bitbucket.org/prodhub/adw-sonata-media-extra-bundle.git"
            }
        ]
    }

### FAQ ###

#### The service "@some_wildfowl" has a dependency on a non-existent service "adw_sonata_media_extra.filesystem.sftp".

Вы хотите использовать CDN с помощью Sftp адаптера, но не указали насройки для адаптера и CDN-сервера в конфиге бандла.

    adw_sonata_media_extra:
        filesystem:
            sftp:
                host:       'welcome2hell.ru'
                username:   'user666'
                password:   'kill-them-all'
                directory:  '/home/user666/public_html/web/uploads/media_test_cdn'
        cdn:
            sftp:
                path:       'http://welcome2hell.ru/uploads/media_test_cdn'

#### Добавил конфигурацию бандла, настроил и проверил подключение к AWS. Файлы заливаются, обрабатываются транскодером, но по прямой ссылке не доступны.

**Ответ**: найти корзину output, открыть ее свойства, открыть вкладку "Permissions", нажать "Add bucket policy", добавить содержимое типа
    
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AddPerm",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "s3:GetObject",
                "Resource": "arn:aws:s3:::YOUR_OUTPUT_BUCKET_NAME/*"
            }
        ]
    }

Внимание! Звено "Version" отмечает версию парсера для данного файла, а не вашу версию данного файла. Другими словами, дату не менять - она всегда должна быть "2012-10-17" такой.

#### Как настроить FFmpeg на Centos 6?
    
**Ответ**: выполнить команды
    
    rpm --import https://raw.githubusercontent.com/example42/puppet-yum/master/files/CentOS.6/rpm-gpg/RPM-GPG-KEY.atrpms
    rpm -Uvh https://www.mirrorservice.org/sites/dl.atrpms.net/el6.7-x86_64/atrpms/stable/atrpms-repo-6-7.el6.x86_64.rpm
    sed -i 's,http://dl,https://www.mirrorservice.org/sites/dl,' /etc/yum.repos.d/atrpms*.repo
    yum install ffmpeg ffmpeg-compat ffmpeg-compat-devel ffmpeg-devel ffmpeg-libs
    
    git clone https://github.com/tony2001/ffmpeg-php.git
    yum install php-devel gcc
    cd ffmpeg-php
    phpize
    ./configure
    make && make install
    
Centos 7
    
    rpm --import https://raw.githubusercontent.com/example42/puppet-yum/master/files/CentOS.7/rpm-gpg/RPM-GPG-KEY.atrpms
    rpm -Uvh https://www.mirrorservice.org/sites/dl.atrpms.net/el7-x86_64/atrpms/stable/atrpms-repo-7-7.el7.x86_64.rpm
    rpm -Uvh ftp://195.220.108.108/linux/atrpms/f21-x86_64/atrpms/stable/libx264_142-0.142-20_20140406.2245.fc20_80.x86_64.rpm
    
    rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
    rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-1.el7.nux.noarch.rpm
    
    yum install ffmpeg ffmpeg-compat ffmpeg-compat-devel ffmpeg-devel ffmpeg-libs
    
    git clone https://github.com/tony2001/ffmpeg-php.git
    yum install php7.0w-devel gcc
    cd ffmpeg-php
    phpize
    ./configure
    make && make install



### References ###

* [Setup AWS credentials](http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html)