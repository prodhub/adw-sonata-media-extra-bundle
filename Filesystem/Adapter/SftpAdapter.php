<?php

namespace ADW\SonataMediaExtraBundle\Filesystem\Adapter;

use Gaufrette\Adapter;
use Gaufrette\Adapter\FileFactory;
use Gaufrette\Adapter\ListKeysAware;
use Gaufrette\File;
use Gaufrette\Filesystem;
use Monolog\Logger;
use phpseclib\Net\SFTP as Net_SFTP;

class SftpAdapter implements Adapter, FileFactory, ListKeysAware
{
    /** @var string */
    private $host;

    /** @var integer */
    private $port;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $directory;

    /** @var boolean */
    private $create;

    /** @var Net_SFTP */
    private $sftp;

    /** @var Logger */
    private $logger;

    /**
     * Sftp constructor.
     * @param string $host
     * @param int $port
     * @param string $username
     * @param string $password
     * @param string $directory
     * @param bool $create
     */
    public function __construct($host, $port, $username, $password, $directory, $create = false)
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->directory = $directory;
        $this->create = $create;
    }

    /**
     * Initialize SFTP connection
     * @throws \Exception
     */
    private function init()
    {
        $this->sftp = new Net_SFTP($this->host, $this->port);
        if (!$this->sftp->login($this->username, $this->password)) {
            $this->logger->error('SFTP Error:' . $this->sftp->getLastError() . ' ' . $this->sftp->getLastSFTPError());
            throw new \Exception("Could not authenticate with \"{$this->username}\".");
        }
    }

    /**
     * {@inheritDoc}
     */
    public function read($key)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $link = $this->fullPath($key);

        return $this->sftp->readlink($link);
    }

    /**
     * @inheritdoc
     */
    public function write($key, $content)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $path = $this->fullPath($key);
        $directory = dirname($path);
        $this->ensureDirectoryExists($directory, true);

        $path = $this->fullPath($key);
        $this->sftp->put($path, $content);

        return $this->sftp->size($path);
    }

    /**
     * @inheritdoc
     */
    public function exists($key)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $link = $this->fullPath($key);

        return $this->sftp->file_exists($link);
    }

    /**
     * @inheritdoc
     */
    public function keys()
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        return $this->listKeys();
    }

    /**
     * @inheritdoc
     */
    public function mtime($key)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        return $this->sftp->mkdir($this->fullPath($key));
    }

    /**
     * @inheritdoc
     */
    public function delete($key)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        return $this->sftp->delete($this->fullPath($key));
    }

    /**
     * @inheritdoc
     */
    public function rename($sourceKey, $targetKey)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $sourcePath = $this->fullPath($sourceKey);
        $targetPath = $this->fullPath($targetKey);

        return $this->sftp->rename($sourcePath, $targetPath);
    }

    /**
     * @inheritdoc
     */
    public function isDirectory($key)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        return $this->sftp->is_dir($this->fullPath($key));
    }

    /**
     * Creates a new File instance and returns it
     *
     * @param string $key
     * @param Filesystem $filesystem
     * @return File
     * @throws \Exception
     */
    public function createFile($key, Filesystem $filesystem)
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $link = $this->fullPath($key);

        $file = new File($key, $filesystem);

//        $content = $this->sftp->readlink($link);
//        $file->setContent($content);

        $size = $this->sftp->filesize($link);
        $file->setSize($size);

        $parts = explode('/', $key);
        $name = $parts[count($parts) - 1];
        $file->setName($name);

        return $file;
    }

    /**
     * Lists keys beginning with pattern given
     * (no wildcard / regex matching)
     *
     * @param string $prefix
     * @return array
     */
    public function listKeys($prefix = '')
    {
        $this->ensureDirectoryExists($this->directory, $this->create);

        $link = $this->fullPath($prefix);
        $output = $this->sftp->exec("ls -da1 {$link}/*");
        $keysList = explode("\n", $output);
//        foreach ($keysList as $key => $item) {
//            if (in_array($item, ['.', '..'])) {
//                unset($keysList[$key]);
//            }
//        }
//        $keysList = array_values($keysList);

        return $keysList;
    }

    /**
     * @param $directory
     * @param bool $create
     */
    protected function ensureDirectoryExists($directory, $create = false)
    {
        $this->init();
        if (!$this->isDir($directory)) {
            if (!$create) {
                throw new \RuntimeException(sprintf('The directory \'%s\' does not exist.', $directory));
            }

            if (!$this->createDirectory($directory)) {
                throw new \RuntimeException(sprintf('Can\'t create directory \'%s\'.', $directory));
            }
        }
    }

    /**
     * @param  string $directory - full directory path
     * @return boolean
     */
    private function isDir($directory)
    {
        if ('/' === $directory) {
            return true;
        }

        return $this->sftp->is_dir($directory);
    }

    /**
     * @param string $directory
     * @param bool $recursive
     * @return bool
     */
    protected function createDirectory($directory, $recursive = true)
    {
        return $this->sftp->mkdir($directory, -1, $recursive);
    }

    /**
     * Computes the path for the given key
     *
     * @param string $key
     * @return string
     */
    private function fullPath($key)
    {
        return rtrim($this->directory, '/') . '/' . $key;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }
}