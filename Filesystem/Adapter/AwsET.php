<?php

namespace ADW\SonataMediaExtraBundle\Filesystem\Adapter;

use ADW\SonataMediaExtraBundle\Exception\AwsS3BucketNotFoundException;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Aws\S3\S3Client;
use Gaufrette\Adapter\AwsS3;

class AwsET extends AwsS3
{
    protected $bucketOutput;

    /**
     * AwsET constructor.
     * @param S3Client $service
     * @param $bucketInput
     * @param $bucketOutput
     * @param array $options
     * @param bool $detectContentType
     */
    public function __construct(
        S3Client $service,
        $bucketInput,
        $bucketOutput,
        array $options = array(),
        $detectContentType = false
    ) {
        parent::__construct($service, $bucketInput, $options, $detectContentType);
        $this->bucketOutput = $bucketOutput;
    }

    /**
     * @inheritdoc
     */
    public function write($key, $content)
    {
        $this->ensureBucketExists();

        $result = parent::write($key, $content);

        $extension = $this->options['preset']['container'];
        $dotPos = strrpos($key, '.');
        $outputKey = substr($key, 0, $dotPos + 1) . $extension;

        $presetId = $this->options['preset']['id'];

        /** @var ElasticTranscoderClient $client */
        $client = ElasticTranscoderClient::factory(array(
            'credentials' => $this->options['credentials'],
            'version' => $this->options['version'],
            'region' => $this->options['region'],
        ));
        $client->createJob([
            'PipelineId' => $this->options['pipeline'],
            'Input' => array(
                'Key' => $key,
            ),
            'Output' => array(
                'Key' => $outputKey,
                'PresetId' => $presetId,
            ),
        ]);

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function deleteInput($key)
    {
        $options = $this->getOptionsInput($key);
//        dump($options);
        try {
            $this->service->deleteObject($options);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteOutput($key)
    {
        $options = $this->getOptionsOutput($key);
//        dump($options);
        try {
            $this->service->deleteObject($options);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function exists($key)
    {
        return $this->service->doesObjectExist($this->bucketOutput, $this->computePath($key));
    }

    /**
     * {@inheritDoc}
     */
    public function existsInput($key)
    {
        return parent::exists($key);
    }

    /**
     * @param string $key
     * @param array $options
     * @return array
     */
    protected function getOptionsInput($key, array $options = array())
    {
        return parent::getOptions($key, $options);
    }

    /**
     * @param string $key
     * @param array $options
     * @return array
     */
    protected function getOptionsOutput($key, array $options = array())
    {
        return array_merge(
            parent::getOptions($key, $options),
            ['Bucket' => $this->bucketOutput]
        );
    }
}