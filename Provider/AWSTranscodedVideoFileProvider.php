<?php

namespace ADW\SonataMediaExtraBundle\Provider;

use ADW\SonataMediaExtraBundle\ADWSonataMediaExtraBundle;
use ADW\SonataMediaExtraBundle\Filesystem\Adapter\AwsET;
use ffmpeg_movie;
use Gaufrette\Filesystem;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\MediaBundle\CDN\CDNInterface;
use Sonata\MediaBundle\Generator\GeneratorInterface;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\FileProvider;
use Sonata\MediaBundle\Thumbnail\ThumbnailInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Symfony\Component\DependencyInjection\ContainerInterface;


class AWSTranscodedVideoFileProvider extends FileProvider
{
    /** @var array */
    private $adwEtOptions = [];

    /** @var array */
    private $presets = [];

    /**
     * @param string $name
     * @param Filesystem $filesystem
     * @param CDNInterface $cdn
     * @param GeneratorInterface $pathGenerator
     * @param ThumbnailInterface $thumbnail
     * @param array $allowedExtensions
     * @param array $allowedMimeTypes
     * @param MetadataBuilderInterface $metadata
     * @param array $adwEtOptions
     * @param array $presets
     */
    public function __construct(
        $name,
        Filesystem $filesystem,
        CDNInterface $cdn,
        GeneratorInterface $pathGenerator,
        ThumbnailInterface $thumbnail,
        array $allowedExtensions = array(),
        array $allowedMimeTypes = array(),
        MetadataBuilderInterface $metadata = null,
        array $adwEtOptions,
        array $presets
    )
    {
        parent::__construct(
            $name,
            $filesystem, $cdn, $pathGenerator,
            $thumbnail,
            $allowedExtensions, $allowedMimeTypes,
            $metadata
        );
        $this->adwEtOptions = $adwEtOptions;
        $this->presets = $presets;
    }

    public function requireThumbnails()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getReferenceImage(MediaInterface $media)
    {
        $providerReference = $media->getProviderReference();

        $presetName = $this->adwEtOptions['preset'];
        $extension = $this->presets[$presetName]['container'];
        $dotPos = strrpos($providerReference, '.');
        $providerReference = substr($providerReference, 0, $dotPos + 1) . $extension;

        $fullPath = sprintf('%s/%s',
            $this->generatePath($media),
            $providerReference
        );

        return $fullPath;
    }

    /**
     * {@inheritdoc}
     */
    public function generatePublicUrl(MediaInterface $media, $format)
    {
        $filename = pathinfo($media->getProviderReference(), PATHINFO_FILENAME);
        $localpath = $this->adwEtOptions['web_local_path'] . '/' . $this->generatePath($media);

        if (!in_array($format, ['reference', 'admin'])) {
            throw new \Exception('ADW ET not provide ability to use formats!');
        }

        if ('admin' == $format) {
            return $localpath . '/' . $filename . '.png';
        }

        $generatePublicUrl = parent::generatePublicUrl($media, $format);

        if (false == $media->getEnabled()) {
            $ad = $this->getFilesystemAdapter();
            $key = $this->getReferenceImage($media);
            $media->setEnabled($ad->exists($key));
        }

        return $generatePublicUrl;
    }

    public function prePersist(MediaInterface $media)
    {
        parent::prePersist($media);

        $binaryContent = $media->getBinaryContent();

        if (null != $binaryContent && $binaryContent instanceof UploadedFile) {
            $mov = new ffmpeg_movie($binaryContent->getRealPath(), false);

            $this->collectBaseMetaData($media, $mov);
            $this->collectMetaData($media, $mov);
            $this->saveScreenshot($media, $mov);

            // TODO: set content type after transcoder execute job
        }
    }

    protected function saveScreenshot($media, $mov)
    {
        $path = $this->adwEtOptions['local_path'] . '/' . $this->generatePath($media) . '/';
        $filename = pathinfo($media->getProviderReference(), PATHINFO_FILENAME);

        $resource = $mov->getFrame([ADWSonataMediaExtraBundle::CUT_VIDEO_FRAME_NUMBER]);
        $resource->resize(ADWSonataMediaExtraBundle::RESIZE_SCREENSHOT_FRAME_WIDTH, ADWSonataMediaExtraBundle::RESIZE_SCREENSHOT_FRAME_HEIGHT);
        $image = $resource->toGDImage();

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        imagepng($image, $path . $filename . '.png');
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove(MediaInterface $media)
    {
        /** @var AwsET $ad */
        $ad = $this->getFilesystem()->getAdapter();

        $sourcePath = $this->generatePath($media) . '/' . $media->getProviderReference();
        $ad->deleteInput($sourcePath);

        $codedPath = $this->getReferenceImage($media);
        $ad->deleteOutput($codedPath);

        if ($this->requireThumbnails()) {
            $this->thumbnail->delete($this, $media);
        }
    }

    public function validate(ErrorElement $errorElement, MediaInterface $media)
    {
//        dump($errorElement);die;
        return parent::validate($errorElement, $media);
    }

    /**
     * @return \Gaufrette\Adapter|AwsET
     */
    protected function getFilesystemAdapter()
    {
        return $this->getFilesystem()->getAdapter();
    }

    /**
     * @param MediaInterface $media
     * @param $mov
     */
    private function collectBaseMetaData(MediaInterface $media, ffmpeg_movie $mov)
    {
        $media->setWidth($mov->getFrameWidth());
        $media->setHeight($mov->getFrameHeight());
        $media->setLength($mov->getDuration());
    }

    /**
     * @param MediaInterface $media
     * @param $mov
     */
    private function collectMetaData(MediaInterface $media, ffmpeg_movie $mov)
    {
        $media->setMetadataValue('BitRate', $mov->getBitRate());
        $media->setMetadataValue('VideoBitRate', $mov->getVideoBitRate());

        if ($mov->hasAudio()) {
            $media->setMetadataValue('AudioBitRate', $mov->getAudioBitRate());
        }
    }
}